package com.example.practica01;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button btnPulsar;
    private Button btnClean;
    private Button btnClose;
    private EditText txtNombre;
    private TextView lblSaludar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // relacionar los objetos
        btnPulsar = (Button) findViewById(R.id.btnSaludar);
        txtNombre = (EditText) findViewById(R.id.txtNombre);
        lblSaludar= (TextView) findViewById(R.id.lblSaludar);
        btnClean = (Button) findViewById(R.id.btnLimpiar);
        btnClose = (Button) findViewById(R.id.btnCerrar);

        //codificar el evento clic del botón

        btnPulsar.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                //Validar
                if(txtNombre.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this,"Faltó capturar la información", Toast.LENGTH_SHORT).show();
                }
                else{
                    String str = "Hola " + txtNombre.getText().toString() + " ¿Cómo estás?";
                    lblSaludar.setText(str.toString());
                }
            }
        });

        btnClean.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtNombre.setText("");
                lblSaludar.setText("");
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}